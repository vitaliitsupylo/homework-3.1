import React from "react";
import PropTypes from 'prop-types';


function Button({type, handler, children, style}) {
    return (
        <button type={type} style={style} onClick={handler}>{children}</button>
    )
}

Button.propTypes = {
    children: PropTypes.string.isRequired,
    handler: PropTypes.func.isRequired,
    type: PropTypes.oneOf(['button', 'submit']).isRequired

};

Button.defaultProps = {
    type: 'button',
    handler: () => console.log('handler'),
    children: 'Button',
    style: {
        backgroundColor: '#000',
        color: '#fff',
        margin: '20px auto',
        height: '40px',
        padding: '0 20px',
        border: 'none'
    }
};

export default Button;