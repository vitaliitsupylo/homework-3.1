import React, {Component} from 'react';
import Button from "./../Button";
import Chart from 'chart.js';

class ChartComponent extends Component {

    state = {
        data: [0, 10, 5, 2, 20, 30, 45]
    };

    canvas = React.createRef();

    componentDidMount() {
        this.chart = new Chart(this.canvas.current.getContext('2d'), {
            type: 'line',
            data: {
                labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
                datasets: [{
                    label: 'My First dataset',
                    backgroundColor: 'rgb(255, 99, 132)',
                    borderColor: 'rgb(255, 99, 132)',
                    data: this.state.data
                }]
            },
            options: {}
        });
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (JSON.stringify(this.state.data) !== JSON.stringify(prevState.data)) {
            this.chart.data.datasets[0].data = this.state.data;
            this.chart.update();
        }
    }

    randomizeData = (count = 7, min = 0, max = 50) => (even) => {
        const arr = new Array(count);
        for (let i = 0; i < arr.length; i++) {
            arr[i] = Math.floor(Math.random() * (max - min + 1)) + min;
        }
        this.setState({data: arr});
    };


    render() {
        const {randomizeData} = this;
        return (
            <div className="chart">
                <canvas ref={this.canvas}/>
                <Button type="button" handler={randomizeData(7, 0, 40)}>
                    Randomize Data</Button>
            </div>
        )
    }
}

export default ChartComponent;
